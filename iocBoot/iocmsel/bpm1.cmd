#!../../bin/linux-x86_64/msel

## You may have to change msel to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/msel.dbd"
msel_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/msel.db","BL=PINK,DEV=BPM1,MOTOR=BPMY01U112L:AbsM1")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=BPM1")

## Start any sequence programs
#seq sncxxx,"user=epics"
